/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 Localization and mapping program using Normal Distributions Transform

 Yuki KITSUKAWA
 */

#define OUTPUT  // If you want to output "position_log.txt", "#define OUTPUT".

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <mutex>
#include <atomic>

#include <boost/format.hpp>
#include <boost/filesystem.hpp>

#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float32.h>
#include <velodyne_pointcloud/point_types.h>
#include <velodyne_pointcloud/rawdata.h>

#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>

#include <tf2/transform_datatypes.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_eigen/tf2_eigen.h>

#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

#ifdef USE_PCL_OPENMP
#include <pcl_omp_registration/ndt.h>
#endif

#include <pcl/filters/voxel_grid.h>
#include <pcl/registration/ndt.h>

#include <autoware_config_msgs/ConfigApproximateNDTMapping.h>
#include <autoware_config_msgs/ConfigNDTMappingOutput.h>

template<typename T>
void save_odometry(const std::string& odometry_directory, const T data);

template<>
void save_odometry(const std::string& odometry_directory, const sensor_msgs::PointCloud2 data) {
  std::stringstream dst_filename;
  dst_filename << odometry_directory << "/" << data.header.stamp.sec << "_" << boost::format("%09d") % data.header.stamp.nsec << ".pcd";

  pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>());
  pcl::fromROSMsg(data, *cloud);

  pcl::io::savePCDFileBinary(dst_filename.str(), *cloud);
}

template<>
void save_odometry(const std::string& odometry_directory, const nav_msgs::Odometry data) {
  std::stringstream dst_filename;
  dst_filename << odometry_directory << "/" << data.header.stamp.sec << "_" << boost::format("%09d") % data.header.stamp.nsec << ".odom";

  const auto& pose = data.pose.pose;

  Eigen::Isometry3d odom = Eigen::Isometry3d::Identity();
  odom.translation() = Eigen::Vector3d(pose.position.x, pose.position.y, pose.position.z);
  odom.linear() = Eigen::Quaterniond(pose.orientation.w, pose.orientation.x, pose.orientation.y, pose.orientation.z).normalized().toRotationMatrix();

  std::ofstream ofs(dst_filename.str());
  ofs << odom.matrix();
  ofs.close();
}

struct pose
{
  double x;
  double y;
  double z;
  double roll;
  double pitch;
  double yaw;
};

static std::string odometry_directory = "/tmp/twistetry";

// global variables
static pose previous_pose, guess_pose, guess_pose_imu, guess_pose_twist, guess_pose_imu_twist, current_pose,
    current_pose_imu, current_pose_twist, current_pose_imu_twist, ndt_pose, added_pose, localizer_pose;

static bool current_pose_set = true;
static ros::Time current_scan_time;
static ros::Time previous_scan_time;
static ros::Duration scan_duration;

static double initial_z = 0.0;
static double diff = 0.0;
static double diff_x = 0.0, diff_y = 0.0, diff_z = 0.0, diff_yaw;  // current_pose - previous_pose
static double offset_imu_x, offset_imu_y, offset_imu_z, offset_imu_roll, offset_imu_pitch, offset_imu_yaw;
static double offset_twist_x, offset_twist_y, offset_twist_z, offset_twist_roll, offset_twist_pitch, offset_twist_yaw;
static double offset_imu_twist_x, offset_imu_twist_y, offset_imu_twist_z, offset_imu_twist_roll, offset_imu_twist_pitch,
    offset_imu_twist_yaw;

static double current_velocity_x = 0.0;
static double current_velocity_y = 0.0;
static double current_velocity_z = 0.0;

static double current_velocity_imu_x = 0.0;
static double current_velocity_imu_y = 0.0;
static double current_velocity_imu_z = 0.0;

static pcl::PointCloud<pcl::PointXYZI> map, submap, map_prev;

static pcl::NormalDistributionsTransform<pcl::PointXYZI, pcl::PointXYZI> ndt;

#ifdef USE_PCL_OPENMP
static pcl_omp::NormalDistributionsTransform<pcl::PointXYZI, pcl::PointXYZI> omp_ndt;
#endif

// Default values
static int max_iter = 30;        // Maximum iterations
static float ndt_res = 1.0;      // Resolution
static double step_size = 0.1;   // Step size
static double trans_eps = 0.01;  // Transformation epsilon

// Leaf size of VoxelGrid filter.
static double voxel_leaf_size = 1.5;

static ros::Time callback_start, callback_end, t1_start, t1_end, t2_start, t2_end, t3_start, t3_end, t4_start, t4_end,
    t5_start, t5_end;
static ros::Duration d_callback, d1, d2, d3, d4, d5;

static ros::Publisher ndt_map_pub;

static ros::Publisher current_pose_pub;
static ros::Publisher exe_time_pub_;

static ros::Subscriber initial_pose_sub;

static geometry_msgs::PoseStamped current_pose_msg, guess_pose_msg;

static int initial_scan_loaded = 0;

static double min_scan_range = 5.0;
static double max_scan_range = 200.0;
static double min_add_scan_shift = 1.0;
static double max_submap_size = 100.0;

static bool isMapUpdate = true;
static bool _use_openmp = 0;
static bool _use_imu = 0;
static bool _use_twist = 0;
static bool _imu_upside_down = false;

static std::string _imu_topic = "/imu_raw";

static double fitness_score;

static int submap_num = 0;
static double submap_size = 0.0;

static sensor_msgs::Imu imu;
static geometry_msgs::TwistStamped twist;

static std::ofstream ofs;
static std::string filename;
  

static void param_callback(const autoware_config_msgs::ConfigApproximateNDTMapping::ConstPtr& input)
{
  ndt_res = input->resolution;
  step_size = input->step_size;
  trans_eps = input->trans_epsilon;
  max_iter = input->max_iterations;
  voxel_leaf_size = input->leaf_size;
  min_scan_range = input->min_scan_range;
  max_scan_range = input->max_scan_range;
  min_add_scan_shift = input->min_add_scan_shift;
  max_submap_size = input->max_submap_size;

  std::cout << "param_callback" << std::endl;
  std::cout << "ndt_res: " << ndt_res << std::endl;
  std::cout << "step_size: " << step_size << std::endl;
  std::cout << "trans_epsilon: " << trans_eps << std::endl;
  std::cout << "max_iter: " << max_iter << std::endl;
  std::cout << "voxel_leaf_size: " << voxel_leaf_size << std::endl;
  std::cout << "min_scan_range: " << min_scan_range << std::endl;
  std::cout << "max_scan_range: " << max_scan_range << std::endl;
  std::cout << "min_add_scan_shift: " << min_add_scan_shift << std::endl;
  std::cout << "max_submap_size: " << max_submap_size << std::endl;
}

static bool getTransform(
  const std::string target_frame, const std::string source_frame,
  const geometry_msgs::TransformStamped::Ptr & transform_stamped_ptr)
{

   static tf2_ros::Buffer tf2_buffer_;
   static tf2_ros::TransformListener tf2_listener_(tf2_buffer_);


  if (target_frame == source_frame) {
    transform_stamped_ptr->header.stamp = ros::Time::now();
    transform_stamped_ptr->header.frame_id = target_frame;
    transform_stamped_ptr->child_frame_id = source_frame;
    transform_stamped_ptr->transform.translation.x = 0.0;
    transform_stamped_ptr->transform.translation.y = 0.0;
    transform_stamped_ptr->transform.translation.z = 0.0;
    transform_stamped_ptr->transform.rotation.x = 0.0;
    transform_stamped_ptr->transform.rotation.y = 0.0;
    transform_stamped_ptr->transform.rotation.z = 0.0;
    transform_stamped_ptr->transform.rotation.w = 1.0;
    return true;
  }

  try {
    *transform_stamped_ptr =
      tf2_buffer_.lookupTransform(target_frame, source_frame, ros::Time(0), ros::Duration(1.0));
  } catch (tf2::TransformException & ex) {
    ROS_WARN("%s", ex.what());
    ROS_ERROR("Please publish TF %s to %s", target_frame.c_str(), source_frame.c_str());

    transform_stamped_ptr->header.stamp = ros::Time::now();
    transform_stamped_ptr->header.frame_id = target_frame;
    transform_stamped_ptr->child_frame_id = source_frame;
    transform_stamped_ptr->transform.translation.x = 0.0;
    transform_stamped_ptr->transform.translation.y = 0.0;
    transform_stamped_ptr->transform.translation.z = 0.0;
    transform_stamped_ptr->transform.rotation.x = 0.0;
    transform_stamped_ptr->transform.rotation.y = 0.0;
    transform_stamped_ptr->transform.rotation.z = 0.0;
    transform_stamped_ptr->transform.rotation.w = 1.0;
    return false;
  }
  return true;
}

static void saveMap()
{

    if (submap.size() != 0)
    {
      map = map_prev;
      map_prev.clear();
      submap.clear();
      submap_size = 0.0;
    }
    
    submap_num++;
}

static void publishOdom(const sensor_msgs::PointCloud2::ConstPtr& points)
{

	tf::Quaternion q;	


	// Odometry publisher
	nav_msgs::Odometry odom;
	odom.header.stamp = points->header.stamp;
	odom.header.frame_id = "map"; 
	odom.child_frame_id = "base_link";
	
	q.setRPY(localizer_pose.roll, localizer_pose.pitch, localizer_pose.yaw);
	odom.pose.pose.position.x = localizer_pose.x;
	odom.pose.pose.position.y = localizer_pose.y;
	odom.pose.pose.position.z = localizer_pose.z;
	odom.pose.pose.orientation.x = q.x();
	odom.pose.pose.orientation.y = q.y();
	odom.pose.pose.orientation.z = q.z();
	odom.pose.pose.orientation.w = q.w();					

	save_odometry(odometry_directory, *points);
	save_odometry(odometry_directory, odom);
	
	sensor_msgs::PointCloud2::Ptr map_msg_ptr(new sensor_msgs::PointCloud2);  
	pcl::toROSMsg(submap, *map_msg_ptr);
	map_msg_ptr->header.frame_id = "map";  	
	ndt_map_pub.publish(*map_msg_ptr);	
	
}


static void imu_twist_calc(ros::Time current_time)
{
  static ros::Time previous_time = current_time;
  double diff_time = (current_time - previous_time).toSec();

  double diff_imu_roll = imu.angular_velocity.x * diff_time;
  double diff_imu_pitch = imu.angular_velocity.y * diff_time;
  double diff_imu_yaw = imu.angular_velocity.z * diff_time;

  current_pose_imu_twist.roll += diff_imu_roll;
  current_pose_imu_twist.pitch += diff_imu_pitch;
  current_pose_imu_twist.yaw += diff_imu_yaw;

  double diff_distance = twist.twist.linear.x * diff_time;
  offset_imu_twist_x += diff_distance * cos(-current_pose_imu_twist.pitch) * cos(current_pose_imu_twist.yaw);
  offset_imu_twist_y += diff_distance * cos(-current_pose_imu_twist.pitch) * sin(current_pose_imu_twist.yaw);
  offset_imu_twist_z += diff_distance * sin(-current_pose_imu_twist.pitch);

  offset_imu_twist_roll += diff_imu_roll;
  offset_imu_twist_pitch += diff_imu_pitch;
  offset_imu_twist_yaw += diff_imu_yaw;

  guess_pose_imu_twist.x = previous_pose.x + offset_imu_twist_x;
  guess_pose_imu_twist.y = previous_pose.y + offset_imu_twist_y;
  guess_pose_imu_twist.z = previous_pose.z + offset_imu_twist_z;
  guess_pose_imu_twist.roll = previous_pose.roll + offset_imu_twist_roll;
  guess_pose_imu_twist.pitch = previous_pose.pitch + offset_imu_twist_pitch;
  guess_pose_imu_twist.yaw = previous_pose.yaw + offset_imu_twist_yaw;

  previous_time = current_time;
}

static void twist_calc(ros::Time current_time)
{
  static ros::Time previous_time = current_time;
  double diff_time = (current_time - previous_time).toSec();

  double diff_twist_roll = twist.twist.angular.x * diff_time;
  double diff_twist_pitch = twist.twist.angular.y * diff_time;
  double diff_twist_yaw = twist.twist.angular.z * diff_time;

  current_pose_twist.roll += diff_twist_roll;
  current_pose_twist.pitch += diff_twist_pitch;
  current_pose_twist.yaw += diff_twist_yaw;

  double diff_distance = twist.twist.linear.x * diff_time;
  offset_twist_x += diff_distance * cos(-current_pose_twist.pitch) * cos(current_pose_twist.yaw);
  offset_twist_y += diff_distance * cos(-current_pose_twist.pitch) * sin(current_pose_twist.yaw);
  offset_twist_z += diff_distance * sin(-current_pose_twist.pitch);

  offset_twist_roll += diff_twist_roll;
  offset_twist_pitch += diff_twist_pitch;
  offset_twist_yaw += diff_twist_yaw;

  guess_pose_twist.x = previous_pose.x + offset_twist_x;
  guess_pose_twist.y = previous_pose.y + offset_twist_y;
  guess_pose_twist.z = previous_pose.z + offset_twist_z;
  guess_pose_twist.roll = previous_pose.roll + offset_twist_roll;
  guess_pose_twist.pitch = previous_pose.pitch + offset_twist_pitch;
  guess_pose_twist.yaw = previous_pose.yaw + offset_twist_yaw;

  previous_time = current_time;
}

static void imu_calc(ros::Time current_time)
{
  static ros::Time previous_time = current_time;
  double diff_time = (current_time - previous_time).toSec();

  double diff_imu_roll = imu.angular_velocity.x * diff_time;
  double diff_imu_pitch = imu.angular_velocity.y * diff_time;
  double diff_imu_yaw = imu.angular_velocity.z * diff_time;

  current_pose_imu.roll += diff_imu_roll;
  current_pose_imu.pitch += diff_imu_pitch;
  current_pose_imu.yaw += diff_imu_yaw;

  double accX1 = imu.linear_acceleration.x;
  double accY1 = std::cos(current_pose_imu.roll) * imu.linear_acceleration.y -
                 std::sin(current_pose_imu.roll) * imu.linear_acceleration.z;
  double accZ1 = std::sin(current_pose_imu.roll) * imu.linear_acceleration.y +
                 std::cos(current_pose_imu.roll) * imu.linear_acceleration.z;

  double accX2 = std::sin(current_pose_imu.pitch) * accZ1 + std::cos(current_pose_imu.pitch) * accX1;
  double accY2 = accY1;
  double accZ2 = std::cos(current_pose_imu.pitch) * accZ1 - std::sin(current_pose_imu.pitch) * accX1;

  double accX = std::cos(current_pose_imu.yaw) * accX2 - std::sin(current_pose_imu.yaw) * accY2;
  double accY = std::sin(current_pose_imu.yaw) * accX2 + std::cos(current_pose_imu.yaw) * accY2;
  double accZ = accZ2;

  offset_imu_x += current_velocity_imu_x * diff_time + accX * diff_time * diff_time / 2.0;
  offset_imu_y += current_velocity_imu_y * diff_time + accY * diff_time * diff_time / 2.0;
  offset_imu_z += current_velocity_imu_z * diff_time + accZ * diff_time * diff_time / 2.0;

  current_velocity_imu_x += accX * diff_time;
  current_velocity_imu_y += accY * diff_time;
  current_velocity_imu_z += accZ * diff_time;

  offset_imu_roll += diff_imu_roll;
  offset_imu_pitch += diff_imu_pitch;
  offset_imu_yaw += diff_imu_yaw;

  guess_pose_imu.x = previous_pose.x + offset_imu_x;
  guess_pose_imu.y = previous_pose.y + offset_imu_y;
  guess_pose_imu.z = previous_pose.z + offset_imu_z;
  guess_pose_imu.roll = previous_pose.roll + offset_imu_roll;
  guess_pose_imu.pitch = previous_pose.pitch + offset_imu_pitch;
  guess_pose_imu.yaw = previous_pose.yaw + offset_imu_yaw;

  previous_time = current_time;
}

static double wrapToPm(double a_num, const double a_max)
{
  if (a_num >= a_max)
  {
    a_num -= 2.0 * a_max;
  }
  return a_num;
}

static double wrapToPmPi(double a_angle_rad)
{
  return wrapToPm(a_angle_rad, M_PI);
}

static void twist_callback(const geometry_msgs::TwistStamped::ConstPtr& input)
{
  // std::cout << __func__ << std::endl;

  twist = *input;
  twist_calc(input->header.stamp);
}

static void initialPoseCallback(const geometry_msgs::PoseStamped::ConstPtr& input)
{

	if (current_pose_set == true){
		return;
	}
	
  previous_pose.x = input->pose.position.x;
  previous_pose.y = input->pose.position.y;
  previous_pose.z = input->pose.position.z;
  
  initial_z = input->pose.position.z;
  localizer_pose.z = initial_z; 
  
  tf2::Quaternion q;
  tf2::fromMsg(input->pose.orientation, q);
  
  tf2::Matrix3x3(q).getRPY(previous_pose.roll, previous_pose.pitch, previous_pose.yaw);
  previous_pose.roll = 0;
  previous_pose.pitch = 0;
  
  current_pose = previous_pose;
  
  current_pose_imu = current_pose;
  current_pose_twist = current_pose;
  current_pose_imu_twist = current_pose;  

  current_pose_set = true;
  initial_pose_sub.shutdown();

}

static void imuUpsideDown(const sensor_msgs::Imu::Ptr input)
{
  double input_roll, input_pitch, input_yaw;

  tf::Quaternion input_orientation;
  tf::quaternionMsgToTF(input->orientation, input_orientation);
  tf::Matrix3x3(input_orientation).getRPY(input_roll, input_pitch, input_yaw);

  input->angular_velocity.x *= -1;
  input->angular_velocity.y *= -1;
  input->angular_velocity.z *= -1;

  input->linear_acceleration.x *= -1;
  input->linear_acceleration.y *= -1;
  input->linear_acceleration.z *= -1;

  input_roll *= -1;
  input_pitch *= -1;
  input_yaw *= -1;

  input->orientation = tf::createQuaternionMsgFromRollPitchYaw(input_roll, input_pitch, input_yaw);
}

static void imu_callback(const sensor_msgs::Imu::Ptr& input)
{
  // std::cout << __func__ << std::endl;

  if (_imu_upside_down)
    imuUpsideDown(input);

  const ros::Time current_time = input->header.stamp;
  static ros::Time previous_time = current_time;
  const double diff_time = (current_time - previous_time).toSec();

  double imu_roll, imu_pitch, imu_yaw;
  tf::Quaternion imu_orientation;
  tf::quaternionMsgToTF(input->orientation, imu_orientation);
  tf::Matrix3x3(imu_orientation).getRPY(imu_roll, imu_pitch, imu_yaw);

  imu_roll = wrapToPmPi(imu_roll);
  imu_pitch = wrapToPmPi(imu_pitch);
  imu_yaw = wrapToPmPi(imu_yaw);

  static double previous_imu_roll = imu_roll, previous_imu_pitch = imu_pitch, previous_imu_yaw = imu_yaw;
  const double diff_imu_roll = imu_roll - previous_imu_roll;

  const double diff_imu_pitch = imu_pitch - previous_imu_pitch;

  double diff_imu_yaw;
  if (fabs(imu_yaw - previous_imu_yaw) > M_PI)
  {
    if (imu_yaw > 0)
      diff_imu_yaw = (imu_yaw - previous_imu_yaw) - M_PI * 2;
    else
      diff_imu_yaw = -M_PI * 2 - (imu_yaw - previous_imu_yaw);
  }
  else
    diff_imu_yaw = imu_yaw - previous_imu_yaw;

  imu.header = input->header;
  imu.linear_acceleration.x = input->linear_acceleration.x;
  // imu.linear_acceleration.y = input->linear_acceleration.y;
  // imu.linear_acceleration.z = input->linear_acceleration.z;
  imu.linear_acceleration.y = 0;
  imu.linear_acceleration.z = 0;

  if (diff_time != 0)
  {
    imu.angular_velocity.x = diff_imu_roll / diff_time;
    imu.angular_velocity.y = diff_imu_pitch / diff_time;
    imu.angular_velocity.z = diff_imu_yaw / diff_time;
  }
  else
  {
    imu.angular_velocity.x = 0;
    imu.angular_velocity.y = 0;
    imu.angular_velocity.z = 0;
  }

  imu_calc(input->header.stamp);

  previous_time = current_time;
  previous_imu_roll = imu_roll;
  previous_imu_pitch = imu_pitch;
  previous_imu_yaw = imu_yaw;
}

static void points_callback(const sensor_msgs::PointCloud2::ConstPtr& input)
{
  double r;
  pcl::PointXYZI p;
  pcl::PointCloud<pcl::PointXYZI> tmp, scan;
  pcl::PointCloud<pcl::PointXYZI>::Ptr filtered_scan_ptr(new pcl::PointCloud<pcl::PointXYZI>());
  pcl::PointCloud<pcl::PointXYZI>::Ptr transformed_scan_ptr(new pcl::PointCloud<pcl::PointXYZI>());
  tf::Quaternion q;
  
  Eigen::Matrix4f t_base_link(Eigen::Matrix4f::Identity());  
  Eigen::Matrix4f t_localizer(Eigen::Matrix4f::Identity());  
      
  // get TF base to sensor
  geometry_msgs::TransformStamped::Ptr TF_base_to_sensor_ptr(new geometry_msgs::TransformStamped);
  
  std::string base_frame_id = "base_link";
  std::string sensor_frame_id = input->header.frame_id;
    
  if (!getTransform(base_frame_id, sensor_frame_id, TF_base_to_sensor_ptr)){
  	return;
  }
  const Eigen::Affine3d base_to_sensor_affine = tf2::transformToEigen(*TF_base_to_sensor_ptr);
  const Eigen::Matrix4f tf_btol = base_to_sensor_affine.matrix().cast<float>();	  

  geometry_msgs::TransformStamped::Ptr TF_sensor_to_base_ptr(new geometry_msgs::TransformStamped);

  if (!getTransform(sensor_frame_id, base_frame_id, TF_sensor_to_base_ptr)){
  	return;
  }
  const Eigen::Affine3d sensor_to_base_affine = tf2::transformToEigen(*TF_sensor_to_base_ptr);
  const Eigen::Matrix4f tf_ltob = sensor_to_base_affine.matrix().cast<float>();	



  tf::TransformBroadcaster br;
  tf::Transform transform;

  std::chrono::time_point<std::chrono::system_clock> matching_end, matching_start, align_start, align_end;

  matching_start = std::chrono::system_clock::now();

  current_scan_time = input->header.stamp;
  pcl::fromROSMsg(*input, tmp);
  for (pcl::PointCloud<pcl::PointXYZI>::const_iterator item = tmp.begin(); item != tmp.end(); item++)
  {
    p.x = (double)item->x;
    p.y = (double)item->y;
    p.z = (double)item->z;
    p.intensity = (double)item->intensity;

    r = sqrt(pow(p.x, 2.0) + pow(p.y, 2.0));
    if (min_scan_range < r && r < max_scan_range)
    {
      scan.push_back(p);
    }
  }

  pcl::PointCloud<pcl::PointXYZI>::Ptr scan_ptr(new pcl::PointCloud<pcl::PointXYZI>(scan));
  pcl::transformPointCloud(*scan_ptr, *transformed_scan_ptr, tf_btol);

  // Add initial point cloud to velodyne_map
  if (initial_scan_loaded == 0)
  {
    //map += *transformed_scan_ptr;
    submap += *transformed_scan_ptr;
    initial_scan_loaded = 1;
    
    localizer_pose = current_pose;
    localizer_pose.z += tf_btol(2, 3);
    
	#ifdef USE_PCL_OPENMP
	  if (_use_openmp == 1)
		{
		  omp_ndt.setInputTarget(transformed_scan_ptr);
		}
		else {
	#endif
		  ndt.setInputTarget(transformed_scan_ptr);
	#ifdef USE_PCL_OPENMP  
	  }
	#endif
    
    
	publishOdom(input);	
	return;
  }

  // Apply voxelgrid filter
  pcl::VoxelGrid<pcl::PointXYZI> voxel_grid_filter;
  voxel_grid_filter.setLeafSize(voxel_leaf_size, voxel_leaf_size, voxel_leaf_size);
  voxel_grid_filter.setInputCloud(transformed_scan_ptr);
  voxel_grid_filter.filter(*filtered_scan_ptr);
  
#ifdef USE_PCL_OPENMP
  if (_use_openmp == 1)
  {
      omp_ndt.setResolution(ndt_res);
      omp_ndt.setMaximumIterations(max_iter);
      omp_ndt.setStepSize(step_size);
      omp_ndt.setTransformationEpsilon(trans_eps);  
	  omp_ndt.setInputSource(filtered_scan_ptr);
  } else {
#endif
      ndt.setResolution(ndt_res);
      ndt.setMaximumIterations(max_iter);
      ndt.setStepSize(step_size);
      ndt.setTransformationEpsilon(trans_eps);
      ndt.setInputSource(filtered_scan_ptr);   
#ifdef USE_PCL_OPENMP  
  }
#endif  

  guess_pose.x = previous_pose.x + diff_x;
  guess_pose.y = previous_pose.y + diff_y;
  guess_pose.z = previous_pose.z + diff_z;
  guess_pose.roll = previous_pose.roll;
  guess_pose.pitch = previous_pose.pitch;
  guess_pose.yaw = previous_pose.yaw + diff_yaw;

  if (_use_imu == true && _use_twist == 1)
    imu_twist_calc(current_scan_time);
  if (_use_imu == true && _use_twist == 0)
    imu_calc(current_scan_time);
  if (_use_imu == false && _use_twist == 1)
    twist_calc(current_scan_time);

  pose guess_pose_for_ndt;
  if (_use_imu == true && _use_twist == 1)
    guess_pose_for_ndt = guess_pose_imu_twist;
  else if (_use_imu == true && _use_twist == 0)
    guess_pose_for_ndt = guess_pose_imu;
  else if (_use_imu == false && _use_twist == 1)
    guess_pose_for_ndt = guess_pose_twist;
  else
    guess_pose_for_ndt = guess_pose;


  geometry_msgs::Pose init_guess_pose;
  init_guess_pose.position.x = guess_pose_for_ndt.x;
  init_guess_pose.position.y = guess_pose_for_ndt.y;
  init_guess_pose.position.z = guess_pose_for_ndt.z;
    
  tf2::Quaternion qt;
  qt.setRPY(guess_pose_for_ndt.roll, guess_pose_for_ndt.pitch, guess_pose_for_ndt.yaw);
  init_guess_pose.orientation = tf2::toMsg(qt);
  
  Eigen::Affine3d initial_pose_affine;
  tf2::fromMsg(init_guess_pose, initial_pose_affine);
  const Eigen::Matrix4f init_guess = initial_pose_affine.matrix().cast<float>();

  t3_end = ros::Time::now();
  d3 = t3_end - t3_start;

   align_start = std::chrono::system_clock::now();

  
  bool has_converged;
  int iteration = 0;

  pcl::PointCloud<pcl::PointXYZI>::Ptr output_cloud(new pcl::PointCloud<pcl::PointXYZI>);
#ifdef USE_PCL_OPENMP
  if (_use_openmp == 1)
  {

    omp_ndt.align(*output_cloud, init_guess);
    
    fitness_score = omp_ndt.getFitnessScore();  
    t_base_link = omp_ndt.getFinalTransformation();
    iteration = omp_ndt.getFinalNumIteration();
    has_converged = omp_ndt.hasConverged();
    }
  else
  {
#endif

    ndt.align(*output_cloud, init_guess);

    fitness_score = ndt.getFitnessScore();
    t_base_link = ndt.getFinalTransformation();  
    iteration = ndt.getFinalNumIteration();
    has_converged = ndt.hasConverged();
#ifdef USE_PCL_OPENMP
  }
#endif


  align_end = std::chrono::system_clock::now();
  double align_time = std::chrono::duration_cast<std::chrono::microseconds>(align_end- align_start).count() / 1000.0;
  ROS_INFO("Align time: %f", align_time);

  t_localizer = t_base_link * tf_btol;

  tf::Matrix3x3 mat_l, mat_b;

  mat_l.setValue(static_cast<double>(t_localizer(0, 0)), static_cast<double>(t_localizer(0, 1)),
                 static_cast<double>(t_localizer(0, 2)), static_cast<double>(t_localizer(1, 0)),
                 static_cast<double>(t_localizer(1, 1)), static_cast<double>(t_localizer(1, 2)),
                 static_cast<double>(t_localizer(2, 0)), static_cast<double>(t_localizer(2, 1)),
                 static_cast<double>(t_localizer(2, 2)));

  mat_b.setValue(static_cast<double>(t_base_link(0, 0)), static_cast<double>(t_base_link(0, 1)),
                 static_cast<double>(t_base_link(0, 2)), static_cast<double>(t_base_link(1, 0)),
                 static_cast<double>(t_base_link(1, 1)), static_cast<double>(t_base_link(1, 2)),
                 static_cast<double>(t_base_link(2, 0)), static_cast<double>(t_base_link(2, 1)),
                 static_cast<double>(t_base_link(2, 2)));
  // Update ndt_pose.
  ndt_pose.x = t_base_link(0, 3);
  ndt_pose.y = t_base_link(1, 3);
  ndt_pose.z = t_base_link(2, 3);
  mat_b.getRPY(ndt_pose.roll, ndt_pose.pitch, ndt_pose.yaw, 1);
  
  ndt_pose.z = initial_z;    
  ndt_pose.roll = 0;
  ndt_pose.pitch = 0;   

  localizer_pose.x = t_localizer(0, 3);
  localizer_pose.y = t_localizer(1, 3);
  localizer_pose.z = initial_z + tf_btol(2, 3);
  mat_l.getRPY(localizer_pose.roll, localizer_pose.pitch, localizer_pose.yaw, 1);
  localizer_pose.roll = 0;
  localizer_pose.pitch = 0;  

  current_pose.x = ndt_pose.x;
  current_pose.y = ndt_pose.y;
  current_pose.z = ndt_pose.z;
  current_pose.roll = ndt_pose.roll;
  current_pose.pitch = ndt_pose.pitch;
  current_pose.yaw = ndt_pose.yaw;
  
  q.setRPY(current_pose.roll, current_pose.pitch, current_pose.yaw);
  current_pose_msg.header.frame_id = "map";
  current_pose_msg.header.stamp = current_scan_time;
  current_pose_msg.pose.position.x = current_pose.x;
  current_pose_msg.pose.position.y = current_pose.y;
  current_pose_msg.pose.position.z = current_pose.z;
  current_pose_msg.pose.orientation.x = q.x();
  current_pose_msg.pose.orientation.y = q.y();
  current_pose_msg.pose.orientation.z = q.z();
  current_pose_msg.pose.orientation.w = q.w();

  current_pose_pub.publish(current_pose_msg);  
  
  Eigen::Affine3d current_pose_affine;
  tf2::fromMsg(current_pose_msg.pose, current_pose_affine);
  const Eigen::Matrix4f current_transform_matrix = current_pose_affine.matrix().cast<float>();  
  
  
  pcl::PointCloud<pcl::PointXYZI>::Ptr map_scan_ptr(new pcl::PointCloud<pcl::PointXYZI>);
  pcl::transformPointCloud(*transformed_scan_ptr, *map_scan_ptr, current_transform_matrix);


  transform.setOrigin(tf::Vector3(current_pose.x, current_pose.y, current_pose.z));
  q.setRPY(current_pose.roll, current_pose.pitch, current_pose.yaw);
  transform.setRotation(q);

  br.sendTransform(tf::StampedTransform(transform, current_scan_time, "map", "base_link"));
  
  scan_duration = current_scan_time - previous_scan_time;
  double secs = scan_duration.toSec();

  // Calculate the offset (curren_pos - previous_pos)
  diff_x = current_pose.x - previous_pose.x;
  diff_y = current_pose.y - previous_pose.y;
  diff_z = current_pose.z - previous_pose.z;
  diff_yaw = current_pose.yaw - previous_pose.yaw;
  diff = sqrt(diff_x * diff_x + diff_y * diff_y + diff_z * diff_z);

  current_velocity_x = diff_x / secs;
  current_velocity_y = diff_y / secs;
  current_velocity_z = diff_z / secs;

  current_pose_imu.x = current_pose.x;
  current_pose_imu.y = current_pose.y;
  current_pose_imu.z = current_pose.z;
  current_pose_imu.roll = current_pose.roll;
  current_pose_imu.pitch = current_pose.pitch;
  current_pose_imu.yaw = current_pose.yaw;

  current_pose_twist.x = current_pose.x;
  current_pose_twist.y = current_pose.y;
  current_pose_twist.z = current_pose.z;
  current_pose_twist.roll = current_pose.roll;
  current_pose_twist.pitch = current_pose.pitch;
  current_pose_twist.yaw = current_pose.yaw;

  current_pose_imu_twist.x = current_pose.x;
  current_pose_imu_twist.y = current_pose.y;
  current_pose_imu_twist.z = current_pose.z;
  current_pose_imu_twist.roll = current_pose.roll;
  current_pose_imu_twist.pitch = current_pose.pitch;
  current_pose_imu_twist.yaw = current_pose.yaw;

  current_velocity_imu_x = current_velocity_x;
  current_velocity_imu_y = current_velocity_y;
  current_velocity_imu_z = current_velocity_z;

  // Update position and posture. current_pos -> previous_pos
  previous_pose.x = current_pose.x;
  previous_pose.y = current_pose.y;
  previous_pose.z = current_pose.z;
  previous_pose.roll = current_pose.roll;
  previous_pose.pitch = current_pose.pitch;
  previous_pose.yaw = current_pose.yaw;

  previous_scan_time.sec = current_scan_time.sec;
  previous_scan_time.nsec = current_scan_time.nsec;

  offset_imu_x = 0.0;
  offset_imu_y = 0.0;
  offset_imu_z = 0.0;
  offset_imu_roll = 0.0;
  offset_imu_pitch = 0.0;
  offset_imu_yaw = 0.0;

  offset_twist_x = 0.0;
  offset_twist_y = 0.0;
  offset_twist_z = 0.0;
  offset_twist_roll = 0.0;
  offset_twist_pitch = 0.0;
  offset_twist_yaw = 0.0;

  offset_imu_twist_x = 0.0;
  offset_imu_twist_y = 0.0;
  offset_imu_twist_z = 0.0;
  offset_imu_twist_roll = 0.0;
  offset_imu_twist_pitch = 0.0;
  offset_imu_twist_yaw = 0.0;
  
  // Calculate the shift between added_pos and current_pos
  double shift = sqrt(pow(current_pose.x - added_pose.x, 2.0) + pow(current_pose.y - added_pose.y, 2.0));
  if (shift >= min_add_scan_shift)
  {
  
    pcl::PointCloud<pcl::PointXYZI>::Ptr filtered_map_scan_ptr(new pcl::PointCloud<pcl::PointXYZI>);
    pcl::transformPointCloud(*filtered_scan_ptr, *filtered_map_scan_ptr, current_transform_matrix);  
  	map += *filtered_map_scan_ptr;
	map_prev += *filtered_map_scan_ptr; 
	  
	submap_size += 1;
	submap += *map_scan_ptr;
	added_pose.x = current_pose.x;
	added_pose.y = current_pose.y;
	added_pose.z = current_pose.z;
	added_pose.roll = current_pose.roll;
	added_pose.pitch = current_pose.pitch;
	added_pose.yaw = current_pose.yaw;
	isMapUpdate = true;
	
     pcl::PointCloud<pcl::PointXYZI>::Ptr map_ptr(new pcl::PointCloud<pcl::PointXYZI>(map));	
	
	#ifdef USE_PCL_OPENMP
	  if (_use_openmp == 1)
		{
		  omp_ndt.setInputTarget(map_ptr);
		}
		else {
	#endif
		  ndt.setInputTarget(map_ptr);
	#ifdef USE_PCL_OPENMP  
	  }
	#endif	
	
	publishOdom(input);
	

  }

  if (submap_size >= max_submap_size)
  {
    saveMap();
  }
  
	matching_end = std::chrono::system_clock::now();
	double exe_time = std::chrono::duration_cast<std::chrono::microseconds>(matching_end - matching_start).count() / 1000.0;  

  std_msgs::Float32 exe_time_msg;
  exe_time_msg.data = exe_time;
  exe_time_pub_.publish(exe_time_msg);

  // Write log
  if (!ofs)
  {
    std::cerr << "Could not open " << filename << "." << std::endl;
    exit(1);
  }

  ofs << input->header.seq << ","
      << input->header.stamp << ","
      << input->header.frame_id << ","
      << scan_ptr->size() << ","
      << filtered_scan_ptr->size() << ","
      << std::fixed << std::setprecision(5) << current_pose.x << ","
      << std::fixed << std::setprecision(5) << current_pose.y << ","
      << std::fixed << std::setprecision(5) << current_pose.z << ","
      << current_pose.roll << ","
      << current_pose.pitch << ","
      << current_pose.yaw << ","
      << ndt_res << ","
      << step_size << ","
      << trans_eps << ","
      << max_iter << ","
      << voxel_leaf_size << ","
      << min_scan_range << ","
      << max_scan_range << ","
      << min_add_scan_shift << ","
      << max_submap_size << std::endl;

  std::cout << "-----------------------------------------------------------------" << std::endl;
  std::cout << "Sequence number: " << input->header.seq << std::endl;
  std::cout << "Number of scan points: " << scan_ptr->size() << " points." << std::endl;
  std::cout << "Number of filtered scan points: " << filtered_scan_ptr->size() << " points." << std::endl;
  std::cout << "transformed_scan_ptr: " << transformed_scan_ptr->points.size() << " points." << std::endl;
  std::cout << "map: " << map.points.size() << " points." << std::endl;
  std::cout << "NDT has converged: " << has_converged << std::endl;
  std::cout << "Fitness score: " << fitness_score << std::endl;
  std::cout << "Number of iteration: " << iteration << std::endl;
  std::cout << "(x,y,z,roll,pitch,yaw):" << std::endl;
  std::cout << "(" << current_pose.x << ", " << current_pose.y << ", " << current_pose.z << ", " << current_pose.roll
            << ", " << current_pose.pitch << ", " << current_pose.yaw << ")" << std::endl;
  std::cout << "Transformation Matrix:" << std::endl;
  std::cout << t_base_link << std::endl;
  std::cout << "shift: " << shift << std::endl;
  std::cout << "current submap size: " << submap_size << std::endl;
  std::cout << "-----------------------------------------------------------------" << std::endl;
}



int main(int argc, char** argv)
{

  setvbuf(stdout, NULL, _IONBF, 0);

  previous_pose.x = 0.0;
  previous_pose.y = 0.0;
  previous_pose.z = 0.0;
  previous_pose.roll = 0.0;
  previous_pose.pitch = 0.0;
  previous_pose.yaw = 0.0;

  ndt_pose.x = 0.0;
  ndt_pose.y = 0.0;
  ndt_pose.z = 0.0;
  ndt_pose.roll = 0.0;
  ndt_pose.pitch = 0.0;
  ndt_pose.yaw = 0.0;

  current_pose.x = 0.0;
  current_pose.y = 0.0;
  current_pose.z = 0.0;
  current_pose.roll = 0.0;
  current_pose.pitch = 0.0;
  current_pose.yaw = 0.0;

  current_pose_imu.x = 0.0;
  current_pose_imu.y = 0.0;
  current_pose_imu.z = 0.0;
  current_pose_imu.roll = 0.0;
  current_pose_imu.pitch = 0.0;
  current_pose_imu.yaw = 0.0;

  guess_pose.x = 0.0;
  guess_pose.y = 0.0;
  guess_pose.z = 0.0;
  guess_pose.roll = 0.0;
  guess_pose.pitch = 0.0;
  guess_pose.yaw = 0.0;

  added_pose.x = 0.0;
  added_pose.y = 0.0;
  added_pose.z = 0.0;
  added_pose.roll = 0.0;
  added_pose.pitch = 0.0;
  added_pose.yaw = 0.0;

  diff_x = 0.0;
  diff_y = 0.0;
  diff_z = 0.0;
  diff_yaw = 0.0;

  offset_imu_x = 0.0;
  offset_imu_y = 0.0;
  offset_imu_z = 0.0;
  offset_imu_roll = 0.0;
  offset_imu_pitch = 0.0;
  offset_imu_yaw = 0.0;

  offset_twist_x = 0.0;
  offset_twist_y = 0.0;
  offset_twist_z = 0.0;
  offset_twist_roll = 0.0;
  offset_twist_pitch = 0.0;
  offset_twist_yaw = 0.0;

  offset_imu_twist_x = 0.0;
  offset_imu_twist_y = 0.0;
  offset_imu_twist_z = 0.0;
  offset_imu_twist_roll = 0.0;
  offset_imu_twist_pitch = 0.0;
  offset_imu_twist_yaw = 0.0;

  ros::init(argc, argv, "graph_mapping");

  ros::NodeHandle nh;
  ros::NodeHandle private_nh("~");

  // Set log file name.
  char buffer[80];
  std::time_t now = std::time(NULL);
  std::tm* pnow = std::localtime(&now);
  std::strftime(buffer, 80, "%Y%m%d_%H%M%S", pnow);
  filename = "graph_mapping_" + std::string(buffer) + ".csv";
  ofs.open(filename.c_str(), std::ios::app);

  // write header for log file
  if (!ofs)
  {
    std::cerr << "Could not open " << filename << "." << std::endl;
    exit(1);
  }

  ofs << "input->header.seq" << ","
      << "input->header.stamp" << ","
      << "input->header.frame_id" << ","
      << "scan_ptr->size()" << ","
      << "filtered_scan_ptr->size()" << ","
      << "current_pose.x" << ","
      << "current_pose.y" << ","
      << "current_pose.z" << ","
      << "current_pose.roll" << ","
      << "current_pose.pitch" << ","
      << "current_pose.yaw" << ","
      << "ndt_res" << ","
      << "step_size" << ","
      << "trans_eps" << ","
      << "max_iter" << ","
      << "voxel_leaf_size" << ","
      << "min_scan_range" << ","
      << "max_scan_range" << ","
      << "min_add_scan_shift" << ","
      << "max_submap_size" << std::endl;

  // setting parameters
  private_nh.getParam("use_openmp", _use_openmp);
  private_nh.getParam("use_imu", _use_imu);
  private_nh.getParam("use_twist", _use_twist);
  private_nh.getParam("imu_upside_down", _imu_upside_down);
  private_nh.getParam("imu_topic", _imu_topic);

  
  
  bool use_current_pose = false;
  private_nh.getParam("use_current_pose", use_current_pose);
  

  private_nh.getParam("odometry_directory", odometry_directory);  

  std::cout << "use_openmp: " << _use_openmp << std::endl;
  std::cout << "use_imu: " << _use_imu << std::endl;
  std::cout << "imu_upside_down: " << _imu_upside_down << std::endl;
  std::cout << "use_twist: " << _use_twist << std::endl;
  std::cout << "imu_topic: " << _imu_topic << std::endl;

  map.header.frame_id = "map";

  ndt_map_pub = nh.advertise<sensor_msgs::PointCloud2>("/ndt_map", 1000);
  current_pose_pub = nh.advertise<geometry_msgs::PoseStamped>("/current_pose", 1000);
  
  exe_time_pub_ = nh.advertise<std_msgs::Float32>("/ndt_exe_time_ms", 10);
  

  
  if (use_current_pose){
      current_pose_set = false;
	  initial_pose_sub = nh.subscribe("initial_pose", 1000, initialPoseCallback);
  }
  
  ros::Subscriber param_sub = nh.subscribe("config/graph_mapping", 10, param_callback);
  ros::Subscriber points_sub = nh.subscribe("points_raw", 100000, points_callback);
  ros::Subscriber twist_sub = nh.subscribe("twist", 100000, twist_callback);
  ros::Subscriber imu_sub = nh.subscribe(_imu_topic, 100000, imu_callback);
  

	boost::filesystem::create_directories(odometry_directory);

  ros::spin();
  

  return 0;
}
