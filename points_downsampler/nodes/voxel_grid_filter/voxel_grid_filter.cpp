/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>

#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/voxel_grid.h>

#include "autoware_config_msgs/ConfigVoxelGridFilter.h"

#include <points_downsampler/PointsDownsamplerInfo.h>

#include <chrono>
#include <boost/filesystem.hpp>
#include <sstream>

#include <dynamic_reconfigure/server.h>
#include <points_downsampler/DynamicVoxelGridFilterConfig.h>

#include "points_downsampler.h"
#include "voxel_grid_filter.h"

static void retrieve_config(const ros::NodeHandle &private_nh)
{
    private_nh.getParam("points_topic", points_topic);
    private_nh.getParam("filtered_points_topic", filtered_points_topic);
    private_nh.getParam("output_info_topic", output_info_topic);
    private_nh.getParam("output_log", output_log);
    private_nh.getParam("measurement_range", measurement_range);
    private_nh.getParam("voxel_leaf_size", voxel_leaf_size);
    private_nh.getParam("use_dynamic_leaf_size", use_dynamic_leaf_size);
    private_nh.getParam("voxel_leaf_size_step", voxel_leaf_size_step);
    private_nh.getParam("min_voxel_leaf_size", min_voxel_leaf_size);
    private_nh.getParam("max_voxel_leaf_size", max_voxel_leaf_size);
    private_nh.getParam("min_points_size", min_points_size);
    private_nh.getParam("max_points_size", max_points_size);

    log_configuration("Config received:");
}

static void create_log_file()
{
    char buffer[80];
    std::time_t now = std::time(NULL);
    std::tm *pnow = std::localtime(&now);
    std::strftime(buffer, 80, "%Y%m%d_%H%M%S", pnow);
    std::string logs_path = "/tmp/voxel_grid_filter_logs/";
    if (!boost::filesystem::exists(logs_path))
    {
        boost::filesystem::create_directories(logs_path);
    }

    filename = logs_path + "voxel_grid_filter_" + std::string(buffer) + ".csv";
    ROS_WARN("voxel grid filter log file: %s", filename.c_str());
    ofs.open(filename.c_str(), std::ios::app);
}

static void set_topics(ros::NodeHandle &node_handle)
{
    scan_sub = node_handle.subscribe(points_topic, 10, scan_callback);
    filtered_points_pub = node_handle.advertise<sensor_msgs::PointCloud2>(filtered_points_topic, 10);
    points_downsampler_info_pub = node_handle.advertise<points_downsampler::PointsDownsamplerInfo>(
        output_info_topic, 1000);
}

static void reconfigure_callback(points_downsampler::DynamicVoxelGridFilterConfig &config,
                                 uint32_t level)
{
    boost::mutex::scoped_lock lock(mutex_);
    if (level & USE_DYNAMIC_LEAF_SIZE_LEVEL)
    {
        use_dynamic_leaf_size = config.use_dynamic_leaf_size;
    }
    if (level & MEASUREMENT_RANGE_LEVEL)
    {
        measurement_range = config.measurement_range;
    }
    if ((level & VOXEL_LEAF_SIZE_LEVEL) || use_dynamic_leaf_size == 0)
    {
        voxel_leaf_size = config.voxel_leaf_size;
    }

    if (level & VOXEL_LEAF_SIZE_STEP_LEVEL)
    {
        voxel_leaf_size_step = config.voxel_leaf_size_step;
    }
    if (level & MIN_VOXEL_LEAF_SIZE_LEVEL)
    {
        min_voxel_leaf_size = config.min_voxel_leaf_size;
    }
    if (level & MAX_VOXEL_LEAF_SIZE_LEVEL)
    {
        max_voxel_leaf_size = config.max_voxel_leaf_size;
    }
    if (level & MIN_POINTS_SIZE_LEVEL)
    {
        min_points_size = config.min_points_size;
    }
    if (level & MAX_POINTS_SIZE_LEVEL)
    {
        max_points_size = config.max_points_size;
    }
    // callback executes in another thread and doesn't output to log
    log_configuration("voxel grid filter dynamic reconfiguration:");
}

static void scan_callback(const sensor_msgs::PointCloud2::ConstPtr &input)
{
    boost::mutex::scoped_lock lock(mutex_);
    pcl::PointCloud<pcl::PointXYZI> scan;
    pcl::fromROSMsg(*input, scan);

    if (measurement_range != MAX_MEASUREMENT_RANGE)
    {
        scan = removePointsByRange(scan, 0, measurement_range);
    }

    pcl::PointCloud<pcl::PointXYZI>::Ptr scan_ptr(new pcl::PointCloud<pcl::PointXYZI>(scan));
    pcl::PointCloud<pcl::PointXYZI>::Ptr filtered_scan_ptr(new pcl::PointCloud<pcl::PointXYZI>());

    filter_start = std::chrono::system_clock::now();
    filter_points(scan_ptr, filtered_scan_ptr);
    filter_end = std::chrono::system_clock::now();

    publish_filtered_msg(input, filtered_scan_ptr);
    publish_info_msg(input, scan, scan_ptr, filtered_scan_ptr);

    if (output_log)
    {
        log_downsample_result();
    }
}

static void filter_points(const pcl::PointCloud<pcl::PointXYZI>::Ptr &scan_ptr,
                          pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr)
{
    adjust_leaf_size_bounds();
    apply_voxel_grid_filter(scan_ptr, filtered_scan_ptr);

    if (use_dynamic_leaf_size)
    {
        filter_points_dynamic(scan_ptr, filtered_scan_ptr);
    }
}

static void adjust_leaf_size_bounds()
{
    // if voxel_leaf_size < 0.1 voxel_grid_filter cannot down sample (It is specification in PCL)
    if (voxel_leaf_size < 0.1)
    {
        voxel_leaf_size = 0.1;
    }
    if (min_voxel_leaf_size < 0.1)
    {
        min_voxel_leaf_size = 0.1;
    }

    if (use_dynamic_leaf_size)
    {
        if (voxel_leaf_size < min_voxel_leaf_size)
        {
            voxel_leaf_size = min_voxel_leaf_size;
        }
        if (voxel_leaf_size > max_voxel_leaf_size)
        {
            voxel_leaf_size = max_voxel_leaf_size;
        }
    }
}

static void apply_voxel_grid_filter(const pcl::PointCloud<pcl::PointXYZI>::Ptr &scan_ptr,
                                    pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr)
{
    pcl::VoxelGrid<pcl::PointXYZI> voxel_grid_filter;
    voxel_grid_filter.setLeafSize(voxel_leaf_size, voxel_leaf_size, voxel_leaf_size);
    voxel_grid_filter.setInputCloud(scan_ptr);
    voxel_grid_filter.filter(*filtered_scan_ptr);
}

static void filter_points_dynamic(const pcl::PointCloud<pcl::PointXYZI>::Ptr &scan_ptr,
                                  pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr)
{
    if (filtered_scan_ptr->points.size() < min_points_size)
    {
        increase_points_count(scan_ptr, filtered_scan_ptr);
    }
    else if (filtered_scan_ptr->points.size() > max_points_size)
    {
        decrease_points_count(scan_ptr, filtered_scan_ptr);
    }
}

static void increase_points_count(const pcl::PointCloud<pcl::PointXYZI>::Ptr &scan_ptr,
                                  pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr)
{
    for (; voxel_leaf_size > min_voxel_leaf_size; voxel_leaf_size -= voxel_leaf_size_step)
    {
        apply_voxel_grid_filter(scan_ptr, filtered_scan_ptr);
        if (filtered_scan_ptr->points.size() > min_points_size)
        {
            break;
        }
    }
}

static void decrease_points_count(const pcl::PointCloud<pcl::PointXYZI>::Ptr &scan_ptr,
                                  pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr)
{
    for (; voxel_leaf_size < max_voxel_leaf_size; voxel_leaf_size += voxel_leaf_size_step)
    {
        apply_voxel_grid_filter(scan_ptr, filtered_scan_ptr);
        if (filtered_scan_ptr->points.size() < max_points_size)
        {
            break;
        }
    }
}

static void publish_filtered_msg(const sensor_msgs::PointCloud2::ConstPtr &input,
                                 const pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr)
{
    sensor_msgs::PointCloud2 filtered_msg;

    filtered_msg.header = input->header;
    pcl::toROSMsg(*filtered_scan_ptr, filtered_msg);

    filtered_points_pub.publish(filtered_msg);
}

static void publish_info_msg(const sensor_msgs::PointCloud2::ConstPtr &input,
                             const pcl::PointCloud<pcl::PointXYZI> &scan,
                             const pcl::PointCloud<pcl::PointXYZI>::Ptr &scan_ptr,
                             const pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr)
{
    points_downsampler_info_msg.header = input->header;
    points_downsampler_info_msg.filter_name = "voxel_grid_filter";
    points_downsampler_info_msg.measurement_range = measurement_range;
    points_downsampler_info_msg.original_points_size = scan.size();

    if (voxel_leaf_size >= 0.1)
    {
        points_downsampler_info_msg.filtered_points_size = filtered_scan_ptr->size();
    }
    else
    {
        points_downsampler_info_msg.filtered_points_size = scan_ptr->size();
    }

    points_downsampler_info_msg.original_ring_size = 0;
    points_downsampler_info_msg.filtered_ring_size = 0;
    points_downsampler_info_msg.exe_time =
        std::chrono::duration_cast<std::chrono::microseconds>(filter_end - filter_start).count() / 1000.0;
    points_downsampler_info_pub.publish(points_downsampler_info_msg);
}

static void log_downsample_result()
{
    if (!ofs)
    {
        std::cerr << "Could not open " << filename << "." << std::endl;
        exit(1);
    }
    ofs << points_downsampler_info_msg.header.seq << ","
        << points_downsampler_info_msg.header.stamp << ","
        << points_downsampler_info_msg.header.frame_id << ","
        << points_downsampler_info_msg.filter_name << ","
        << points_downsampler_info_msg.original_points_size << ","
        << points_downsampler_info_msg.filtered_points_size << ","
        << voxel_leaf_size << ","
        << points_downsampler_info_msg.exe_time << ","
        << std::endl;
}

static void log_configuration(const std::string message)
{
    std::stringstream msg_stream;
    
    msg_stream << message << "\n"
        << "  -points_topic: " << points_topic << "\n"
        << "  -filtered_points_topic: " << filtered_points_topic << "\n"
        << "  -output_info_topic: " << output_info_topic << "\n"
        << "  -output_log: " << output_log << "\n"
        << "  -measurement_range: " << measurement_range << "\n"
        << "  -voxel_leaf_size: " << voxel_leaf_size << "\n"
        << "  -use_dynamic_leaf_size: " << use_dynamic_leaf_size << "\n"
        << "  -voxel_leaf_size_step: " << voxel_leaf_size_step << "\n"
        << "  -min_voxel_leaf_size: " << min_voxel_leaf_size << "\n"
        << "  -max_voxel_leaf_size: " << max_voxel_leaf_size << "\n"
        << "  -min_points_size: " << min_points_size << "\n"
        << "  -max_points_size: " << max_points_size << "\n"
        << std::endl;

    ROS_WARN(msg_stream.str().c_str());
    //ofs << msg_stream.str();
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, NODE_NAME);
    ros::NodeHandle node_handle;
    ros::NodeHandle private_nh("~");
    retrieve_config(private_nh);
    set_topics(node_handle);
    if (output_log)
    {
        create_log_file();
    }

    dynamic_reconfigure::Server<points_downsampler::DynamicVoxelGridFilterConfig> server;
    dynamic_reconfigure::Server<points_downsampler::DynamicVoxelGridFilterConfig>::CallbackType f;

    f = boost::bind(&reconfigure_callback, _1, _2);
    server.setCallback(f);

    ros::spin();

    return 0;
}
