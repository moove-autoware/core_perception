#pragma once

#include <string>

#define MAX_MEASUREMENT_RANGE 200.0

const uint32_t MEASUREMENT_RANGE_LEVEL = 1;
const uint32_t VOXEL_LEAF_SIZE_LEVEL = 2;
const uint32_t USE_DYNAMIC_LEAF_SIZE_LEVEL = 4;
const uint32_t VOXEL_LEAF_SIZE_STEP_LEVEL = 8;
const uint32_t MIN_VOXEL_LEAF_SIZE_LEVEL = 16;
const uint32_t MAX_VOXEL_LEAF_SIZE_LEVEL = 32;
const uint32_t MIN_POINTS_SIZE_LEVEL = 64;
const uint32_t MAX_POINTS_SIZE_LEVEL = 128;

const std::string NODE_NAME = "voxel_grid_filter";

static std::string points_topic;
static std::string filtered_points_topic;
static std::string output_info_topic;
static bool output_log = false;
static double measurement_range = MAX_MEASUREMENT_RANGE;

static double voxel_leaf_size = 2.0;
static bool use_dynamic_leaf_size = false;
static double voxel_leaf_size_step = 0.2;
static double min_voxel_leaf_size = 0.2;
static double max_voxel_leaf_size = 3.0;
static int min_points_size = 1500;
static int max_points_size = 2500;

static ros::Subscriber scan_sub;
static ros::Publisher filtered_points_pub;
static ros::Publisher points_downsampler_info_pub;

static points_downsampler::PointsDownsamplerInfo points_downsampler_info_msg;

static std::chrono::time_point<std::chrono::system_clock> filter_start, filter_end;

static std::ofstream ofs;

static std::string filename;

boost::mutex mutex_;

static void retrieve_config(const ros::NodeHandle& private_nh);
static void create_log_file();
static void set_topics(ros::NodeHandle& node_handle);
static void scan_callback(const sensor_msgs::PointCloud2::ConstPtr &input);
static void filter_points(const pcl::PointCloud<pcl::PointXYZI>::Ptr &scan_ptr,
                          pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr);
static void adjust_leaf_size_bounds();
static void apply_voxel_grid_filter(const pcl::PointCloud<pcl::PointXYZI>::Ptr &scan_ptr,
                                    pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr);
static void filter_points_dynamic(const pcl::PointCloud<pcl::PointXYZI>::Ptr &scan_ptr,
                                  pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr);
static void increase_points_count(const pcl::PointCloud<pcl::PointXYZI>::Ptr &scan_ptr,
                                  pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr);
static void decrease_points_count(const pcl::PointCloud<pcl::PointXYZI>::Ptr &scan_ptr,
                                  pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr);
static void publish_filtered_msg(const sensor_msgs::PointCloud2::ConstPtr &input,
                                 const pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr);
static void publish_info_msg(const sensor_msgs::PointCloud2::ConstPtr &input,
                             const pcl::PointCloud<pcl::PointXYZI> &scan,
                             const pcl::PointCloud<pcl::PointXYZI>::Ptr &scan_ptr,
                             const pcl::PointCloud<pcl::PointXYZI>::Ptr &filtered_scan_ptr);
static void log_downsample_result();
static void reconfigure_callback(points_downsampler::DynamicVoxelGridFilterConfig &config,
                                 uint32_t level);
static void log_configuration(const std::string message);