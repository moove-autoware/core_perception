#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <ros/ros.h>
#include <sensor_msgs/NavSatFix.h>
#include <std_msgs/Bool.h>
#include <tf/transform_broadcaster.h>
#include <autoware_msgs/GNSSUtmPose.h>
#include <tf/transform_listener.h>
#include <iostream>
#include <geodesy/utm.h>
#include <sensor_msgs/Imu.h>
#include <math.h>
  
class GpsLocalizer {
	public:
		GpsLocalizer();
	private: 
		tf::TransformListener tf_listener;
		
		ros::Subscriber gnss_pose_subscriber;
		ros::Subscriber imu_subscriber;
				
		ros::Publisher pose_world_publisher;
		ros::Publisher stat_world_publisher;
		
		ros::Publisher stat_publisher;
		ros::Publisher pose_publisher;

		std_msgs::Bool gnss_stat_msg;
		std_msgs::Bool gnss_local_stat_msg;

		//static autoware_msgs::GNSSUtmPose _prev_pose;
		double gnss_yaw;
		double imu_yaw;
		bool has_imu = false;
		tf::Quaternion yaw_q;

		geometry_msgs::PoseStamped _prev_pose;

		tf::TransformBroadcaster br;	
		
		void ImuCallback(const sensor_msgs::ImuConstPtr &msg);
		void GNSSCallback(const sensor_msgs::NavSatFixConstPtr &msg);
		bool getTransformFromTF(std::string child_frame, std::string parent_frame, tf::StampedTransform &transform);
};  

GpsLocalizer::GpsLocalizer() 
{

  ros::NodeHandle nh;
  ros::NodeHandle private_nh("~");
  
  


  //pose_world_publisher = nh.advertise<autoware_msgs::GNSSUtmPose>("gnss_pose/world", 1000);
  pose_publisher = nh.advertise<geometry_msgs::PoseStamped>("gnss_pose", 1000);
  // stat_world_publisher = nh.advertise<std_msgs::Bool>("/gnss_stat/world", 1000);
  stat_publisher = nh.advertise<std_msgs::Bool>("/gnss_stat", 1000);
  gnss_pose_subscriber = nh.subscribe("fix", 100, &GpsLocalizer::GNSSCallback, this);
  imu_subscriber = nh.subscribe("/imu/data_mag", 100, &GpsLocalizer::ImuCallback, this);


}
  
bool GpsLocalizer::getTransformFromTF(std::string child_frame, std::string parent_frame, tf::StampedTransform &transform)
{ 

  try
  {
    tf_listener.lookupTransform(child_frame, parent_frame, ros::Time(0), transform);
    return true;
  }
  catch (tf::TransformException &ex)
  { 
    std::cout << "lookup tranform error from " << child_frame << " to " << parent_frame << std::endl;
  }
  return false;
}

void GpsLocalizer::ImuCallback(const sensor_msgs::ImuConstPtr &msg)
{

	has_imu = true;
	tf::Quaternion q(
   	 	msg->orientation.x,
    	msg->orientation.y,
    	msg->orientation.z,
    	msg->orientation.w
    );
    q.normalize();
    tf::Quaternion q_rot;

	double r= 0, p=0, y=(M_PI * 0.5); 
    q_rot.setRPY(r, p, y);

	yaw_q = q * q_rot;
	yaw_q = q;	
	yaw_q.normalize();

	tf::Matrix3x3 m(yaw_q);
	double roll, pitch, yaw;
	m.getRPY(roll, pitch, yaw);
		
	imu_yaw = yaw;
	
}

void GpsLocalizer::GNSSCallback(const sensor_msgs::NavSatFixConstPtr &msg)
{

  tf::StampedTransform transform_world;
  if (!GpsLocalizer::getTransformFromTF("/map", "/world", transform_world))
  {
    gnss_stat_msg.data = false;
    stat_publisher.publish(gnss_stat_msg);
    return;
  }



  // UTM STATS  
  geographic_msgs::GeoPoint geo_pt;
  geo_pt.latitude = msg->latitude;
  geo_pt.longitude = msg->longitude;
  geo_pt.altitude = msg->altitude;
  geodesy::UTMPoint utm_pt(geo_pt);
  
  geometry_msgs::PoseStamped pose_world;
  pose_world.header = msg->header;
  pose_world.header.frame_id = "world";
  pose_world.pose.position.x = utm_pt.easting;
  pose_world.pose.position.y = utm_pt.northing;
  pose_world.pose.position.z = 0;  
  
  if (pose_world.pose.position.x == 0.0 || pose_world.pose.position.y == 0.0)
  {
    gnss_stat_msg.data = false;
  } 
  else
  {
    gnss_stat_msg.data = true;
  }

	        
 	

	double yaw = has_imu ? imu_yaw : gnss_yaw;

	/* 
	tf::Vector3 v(pose.pose.position.x, pose.pose.position.y, pose.pose.position.z);
	tf::Transform transform(_quat, v);    

	tf::Transform local_tranform = transform * transform_map.inverse();
	*/

	tf::Vector3 v(pose_world.pose.position.x, pose_world.pose.position.y, pose_world.pose.position.z);

	tf::Transform transform;
	transform.setOrigin(v);
	
	if (has_imu){
		transform.setRotation(yaw_q);
		tf::quaternionTFToMsg(yaw_q, pose_world.pose.orientation);
	} else {
	 	gnss_yaw = atan2(pose_world.pose.position.y - _prev_pose.pose.position.y, pose_world.pose.position.x - _prev_pose.pose.position.x);
		transform.setRotation(tf::createQuaternionFromYaw(gnss_yaw));
		pose_world.pose.orientation = tf::createQuaternionMsgFromYaw(gnss_yaw);
	}


	br.sendTransform(tf::StampedTransform(transform, msg->header.stamp, "world", "gps")); 
	 _prev_pose = pose_world;

	geometry_msgs::PoseStamped pose;
	
	if (tf_listener.canTransform("map", "world", ros::Time::now())){
		tf_listener.transformPose("map", pose_world, pose);	
		
		/*
		pose.pose.position.x -= transform_world.getOrigin().getX();
		pose.pose.position.y -= transform_world.getOrigin().getY();
		pose.pose.position.z -= transform_world.getOrigin().getZ();
		
		*/
		pose_publisher.publish(pose);
	}
	
	

	//ROS_INFO("GNSS local position: x = %f, y = %f, z = %f", pose.pose.position.x, pose.pose.position.y, pose.pose.position.z);

  
  	stat_publisher.publish(gnss_stat_msg);

  
  /*

  autoware_msgs::GNSSUtmPose position_msg;
  autoware_msgs::GNSSUtmPose position_msg_local;
  position_msg.header = msg->header;
  // pose.header.stamp = ros::Time::now();
  position_msg.header.frame_id = "world";  
  position_msg.pose_data.pose.position.x = utm_pt.easting;
  position_msg.pose_data.pose.position.y = utm_pt.northing;
  position_msg.pose_data.pose.position.z = utm_pt.altitude;
  position_msg.UTMZone = utm_pt.zone;
  
  position_msg.pose_data.pose.orientation = _quat;
  position_msg_local = position_msg;
  pose_world_publisher.publish(position_msg);
  stat_world_publisher.publish(gnss_stat_msg);

  
  tf::Transform transform;
  tf::Quaternion q;
  transform.setOrigin(tf::Vector3(position_msg.pose_data.pose.position.x, 
                                  position_msg.pose_data.pose.position.y, 
                                  position_msg.pose_data.pose.position.z));
  q.setRPY(0, 0, yaw);
  transform.setRotation(q);
  br.sendTransform(tf::StampedTransform(transform, msg->header.stamp, "world", "gps"));


  std::string child_frame = "world", parent_frame = "map";
  tf::StampedTransform transform_1;
  if (!getTransformFromTF(child_frame, parent_frame, transform_1))
  {
    gnss_local_stat_msg.data = false;
  }
  else {
    auto vec = transform_1.getOrigin();
    position_msg_local.pose_data.pose.position.x -= vec.getX();
    position_msg_local.pose_data.pose.position.y -= vec.getY();
    position_msg_local.pose_data.pose.position.z -= vec.getZ();
    gnss_local_stat_msg.data = true;

    pose_local_publisher.publish(position_msg_local);
    std::cout << "X: " <<  vec.getX() << std::endl;
    std::cout << "Y: " <<  vec.getY() << std::endl;
    std::cout << "Z: " <<  vec.getZ() << std::endl;
  }
  stat_local_publisher.publish(gnss_local_stat_msg);
  
  */
  
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "gps2utmpose");

  GpsLocalizer gps;
  
  ros::spin();
  return 0;
}
