/*
 * Copyright 2017-2020 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************
 *  v1.0: amc-nu (abrahammonrroy@yahoo.com)
 */
#ifndef POINTS_PREPROCESSOR_RAY_GROUND_FILTER_RAY_GROUND_FILTER_H
#define POINTS_PREPROCESSOR_RAY_GROUND_FILTER_RAY_GROUND_FILTER_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <memory>
#include <string>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <velodyne_pointcloud/point_types.h>
#include "autoware_config_msgs/ConfigRayGroundFilter.h"

#include <tf2/transform_datatypes.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_eigen/tf2_eigen.h>

// headers in Autoware Health Checker
#include <autoware_health_checker/health_checker/health_checker.h>
#include <points_preprocessor/DynamicRayGroundFilterConfig.h>

#define USE_ATAN_APPROXIMATION

class RayGroundFilter
{
private:
  std::shared_ptr<autoware_health_checker::HealthChecker> health_checker_ptr_;
  ros::NodeHandle node_handle_;
  ros::Subscriber points_node_sub_;
  ros::Subscriber config_node_sub_;
  ros::Publisher groundless_points_pub_;
  ros::Publisher ground_points_pub_;

  tf2_ros::Buffer tf_buffer_;
  tf2_ros::TransformListener tf_listener_;

  std::string input_point_topic_;
  std::string base_frame_;

  double max_global_slope_deg;   // degrees
  double max_local_slope_deg;    // degrees
  double radial_divider_angle;  // distance in rads between dividers
  double min_height_threshold;  // minimum height threshold regardless the slope, useful for close points
  double min_height;            // maximum height threshold regardless the slope, useful for close points
  double max_height;            // maximum height threshold regardless the slope, useful for close points
  double min_point_distance;    // minimum distance from the origin to consider a point as valid
  double max_global_height_threshold;
  double nonground_retro_threshold;
  double max_last_local_ground_threshold;
  double max_provisional_ground_distance;

  size_t radial_dividers_num;

  /// 'state' member variables
  float prev_radius;
  float prev_height;
  float prev_ground_radius;
  float prev_ground_height;
  bool last_was_ground;

  boost::mutex mutex;

  /// arbitrary small constant: 1.0E-6F
  const float FEPS = 0.000001F;

  struct PointRH
  {
    float height;
    float radius;  // cylindric coords on XY Plane
    void* original_data_pointer;

    PointRH(float height, float radius, void* original_data_pointer)
      : height(height), radius(radius), original_data_pointer(original_data_pointer)
    {
    }
  };
  typedef std::vector<PointRH> PointCloudRH;

  enum class PointLabel : int8_t
  {
    /// \brief Point is ground
    GROUND = 0,
    /// \brief Point is nonground
    NONGROUND = 1,
    /// \brief Last point was nonground. This one is maybe ground. Label is
    /// solidified if terminal or next label is ground, otherwise nonground
    PROVISIONAL_GROUND = -1,
    /// \brief point is so vertical wrt last that last point is also nonground
    RETRO_NONGROUND = 2,
    /// \brief last was provisional ground, but this point is distant
    NONLOCAL_NONGROUND = 3
  };

  const uint32_t MAX_GLOBAL_SLOPE_DEG_LEVEL = 2;
  const uint32_t MAX_LOCAL_SLOPE_DEG_LEVEL = 4;
  const uint32_t NONGROUND_RETRO_THRESH_DEG_LEVEL = 8;
  const uint32_t MIN_HEIGHT_THRESHOLD_LEVEL = 16;
  const uint32_t MAX_GLOBAL_HEIGHT_THRESHOLD_LEVEL = 32;
  const uint32_t MAX_LAST_LOCAL_GROUND_THERSHOLD_LEVEL = 64;
  const uint32_t MAX_PROVISIONAL_GROUND_DISTANCE_LEVEL = 128;
  const uint32_t MIN_HEIGHT_LEVEL = 256;
  const uint32_t MAX_HEIGHT_LEVEL = 512;
  const uint32_t RADIAL_DIVIDER_ANGLE_LEVEL = 1024;
  const uint32_t MIN_POINT_DISTANCE_LEVEL = 2048;

  template <typename T>
  inline T clamp(const T val, const T min, const T max)
  {
    return (val < min) ? min : ((val > max) ? max : val);
  }

  void update_config_params(const autoware_config_msgs::ConfigRayGroundFilter::ConstPtr& param);

  /*!
   * Output transformed PointCloud from in_cloud_ptr->header.frame_id to in_target_frame
   * @param in_target_frame Coordinate system to perform transform
   * @param in_cloud_ptr PointCloud to perform transform
   * @param out_cloud_ptr Resulting transformed PointCloud
   * @retval true transform successed
   * @retval false transform faild
   */
  bool TransformPointCloud(const std::string& in_target_frame, const sensor_msgs::PointCloud2::ConstPtr& in_cloud_ptr,
                           const sensor_msgs::PointCloud2::Ptr& out_cloud_ptr);

  /*!
   * Extract the points pointed by in_selector from in_radial_ordered_clouds to copy them in out_no_ground_ptrs
   * @param pub The ROS publisher on which to output the point cloud
   * @param in_sensor_cloud The input point cloud from which to select the points to publish
   * @param in_selector The pointers to the input cloud's binary blob. No checks are done so be carefull
   */
  void publish(ros::Publisher pub, const sensor_msgs::PointCloud2ConstPtr in_sensor_cloud,
               const std::vector<void*>& in_selector);

  /*!
   * Extract the points pointed by in_selector from in_radial_ordered_clouds to copy them in out_no_ground_ptrs
   * @param in_origin_cloud The original cloud from which we want to copy the points
   * @param in_selector The pointers to the input cloud's binary blob. No checks are done so be carefull
   * @param out_filtered_msg Returns a cloud comprised of the selected points from the origin cloud
   */
  void filterROSMsg(const sensor_msgs::PointCloud2ConstPtr in_origin_cloud, const std::vector<void*>& in_selector,
                    const sensor_msgs::PointCloud2::Ptr out_filtered_msg);

  /*!
   * Classifies Points in the PointCoud as Ground and Not Ground
   * @param in_radial_ordered_clouds Vector of an Ordered PointsCloud ordered by radial distance from the origin
   * @param in_point_count Total number of lidar point. This is used to reserve the output's vector memory
   * @param out_ground_indices Returns the indices of the points classified as ground in the original PointCloud
   * @param out_no_ground_indices Returns the indices of the points classified as not ground in the original PointCloud
   */
  void ClassifyPointCloud(const std::vector<PointCloudRH>& in_radial_ordered_clouds, const size_t in_point_count,
                          std::vector<void*>* out_ground_ptrs, std::vector<void*>* out_no_ground_ptrs);

  void reset_classifier();

  inline float deg2rad(float degrees)
  {
    return degrees * 4.0 * atan(1.0) / 180.0;
  }

  /*!
   * Convert the sensor_msgs::PointCloud2 into PointCloudRH and filter out the points too high or too close
   * @param in_transformed_cloud Input Point Cloud to be organized in radial segments
   * @param in_min_distance Minimum valid distance, points closer than this will be removed.
   * @param out_radial_ordered_clouds Vector of Points Clouds, each element will contain the points ordered
   * @param out_no_ground_ptrs Returns the pointers to the points filtered out as no ground
   */
  void ConvertAndTrim(const sensor_msgs::PointCloud2::Ptr in_transformed_cloud, double in_min_distance,
                      std::vector<PointCloudRH>* out_radial_ordered_clouds, std::vector<void*>* out_no_ground_ptrs);

  void CloudCallback(const sensor_msgs::PointCloud2ConstPtr& in_sensor_cloud);

  /*
   * Decides if point is ground or not based on locality to last point, max local and
   * global slopes, dependent on and updates state
   *
   * @param pt The point to be classified as ground or not ground
   * @retval PointLabel::GROUND if the point is classified as ground
   *
   * PointLabel::NONGROUND if the point is classified as not ground
   * PointLabel::RETRO_NONGROUND if the point is classified as not ground and
   * the point is so vertical that the last point should also be ground
   * throw std::runtime_error if points are not received in order of increasing radius
   */
  PointLabel is_ground(const PointRH& pt);

  void reconfigure_callback(points_preprocessor::DynamicRayGroundFilterConfig& config, uint32_t level);

  /*
   * Whether a points label is abstractly ground or nonground
   * @param label the label to check whether or not its ground
   * @retval true if ground or provisionally ground, false if nonground or retro nonground
   */
  static bool label_is_ground(const PointLabel label);

  friend class RayGroundFilter_callback_Test;

public:
  RayGroundFilter();
  void Run();
};

#endif  // POINTS_PREPROCESSOR_RAY_GROUND_FILTER_RAY_GROUND_FILTER_H
