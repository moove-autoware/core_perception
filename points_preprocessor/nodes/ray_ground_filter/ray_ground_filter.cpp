/*
 * Copyright 2017-2020 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************
 *  v1.0: amc-nu (abrahammonrroy@yahoo.com)
 */
#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/PointField.h>
#include <pcl_ros/transforms.h>
#include <velodyne_pointcloud/point_types.h>

#include "autoware_config_msgs/ConfigRayGroundFilter.h"
#include "points_preprocessor/ray_ground_filter/ray_ground_filter.h"
#include "points_preprocessor/ray_ground_filter/atan2_utils.h"

#include <dynamic_reconfigure/server.h>
#include <points_preprocessor/DynamicRayGroundFilterConfig.h>

void RayGroundFilter::update_config_params(const autoware_config_msgs::ConfigRayGroundFilter::ConstPtr& param)
{
  max_global_slope_deg = param->max_global_slope_deg;
  max_local_slope_deg = param->max_local_slope_deg;
  nonground_retro_threshold = tanf(static_cast<float>(deg2rad(param->nonground_retro_thresh_deg)));
  min_height_threshold = param->min_height_threshold;
  max_global_height_threshold = param->max_global_height_threshold;
  max_last_local_ground_threshold = param->max_last_local_ground_threshold;
  max_provisional_ground_distance = param->max_provisional_ground_distance;
  min_height = param->min_height;
  max_height = param->max_height;

  radial_divider_angle = param->radial_divider_angle;
  min_point_distance = param->min_point_distance;
  radial_dividers_num = ceil(360.0 / radial_divider_angle);

  // todo: maybe necessary add checks for config angles
}

/*!
 * Output transformed PointCloud from in_cloud_ptr->header.frame_id to in_target_frame
 * @param in_target_frame Coordinate system to perform transform
 * @param in_cloud_ptr PointCloud to perform transform
 * @param out_cloud_ptr Resulting transformed PointCloud
 * @retval true transform successed
 * @retval false transform faild
 */
bool RayGroundFilter::TransformPointCloud(const std::string& in_target_frame,
                                          const sensor_msgs::PointCloud2::ConstPtr& in_cloud_ptr,
                                          const sensor_msgs::PointCloud2::Ptr& out_cloud_ptr)
{
  if (in_target_frame == in_cloud_ptr->header.frame_id)
  {
    *out_cloud_ptr = *in_cloud_ptr;
    return true;
  }

  geometry_msgs::TransformStamped transform_stamped;
  try
  {
    transform_stamped = tf_buffer_.lookupTransform(in_target_frame, in_cloud_ptr->header.frame_id,
                                                   in_cloud_ptr->header.stamp, ros::Duration(1.0));
  }
  catch (tf2::TransformException& ex)
  {
    ROS_WARN("%s", ex.what());
    return false;
  }
  // tf2::doTransform(*in_cloud_ptr, *out_cloud_ptr, transform_stamped);
  Eigen::Matrix4f mat = tf2::transformToEigen(transform_stamped.transform).matrix().cast<float>();
  pcl_ros::transformPointCloud(mat, *in_cloud_ptr, *out_cloud_ptr);
  out_cloud_ptr->header.frame_id = in_target_frame;
  return true;
}

/*!
 * Extract the points pointed by in_selector from in_radial_ordered_clouds to copy them in out_no_ground_ptrs
 * @param pub The ROS publisher on which to output the point cloud
 * @param in_sensor_cloud The input point cloud from which to select the points to publish
 * @param in_selector The pointers to the input cloud's binary blob. No checks are done so be carefull
 */
void RayGroundFilter::publish(ros::Publisher pub, const sensor_msgs::PointCloud2ConstPtr in_sensor_cloud,
                              const std::vector<void*>& in_selector)
{
  sensor_msgs::PointCloud2::Ptr output_cloud(new sensor_msgs::PointCloud2);
  filterROSMsg(in_sensor_cloud, in_selector, output_cloud);
  pub.publish(*output_cloud);
}

/*!
 * Extract the points pointed by in_selector from in_radial_ordered_clouds to copy them in out_no_ground_ptrs
 * @param in_origin_cloud The original cloud from which we want to copy the points
 * @param in_selector The pointers to the input cloud's binary blob. No checks are done so be carefull
 * @param out_filtered_msg Returns a cloud comprised of the selected points from the origin cloud
 */
void RayGroundFilter::filterROSMsg(const sensor_msgs::PointCloud2ConstPtr in_origin_cloud,
                                   const std::vector<void*>& in_selector,
                                   const sensor_msgs::PointCloud2::Ptr out_filtered_msg)
{
  size_t point_size = in_origin_cloud->row_step / in_origin_cloud->width;  // in Byte

  // TODO(yoan picchi) I fear this may do a lot of cache miss because it is sorted in the radius
  // and no longer sorted in the original pointer. One thing to try is that, given
  // that we know the value possibles, we can make a rather large vector and insert
  // all the point in, then move things around to remove the "blank" space. This
  // would be a linear sort to allow cache prediction to work better. To be tested.

  size_t data_size = point_size * in_selector.size();
  out_filtered_msg->data.resize(data_size);  // TODO(yoan picchi) a fair amount of time (5-10%) is wasted on this resize

  size_t offset = 0;
  for (auto it = in_selector.cbegin(); it != in_selector.cend(); it++)
  {
    memcpy(out_filtered_msg->data.data() + offset, *it, point_size);
    offset += point_size;
  }

  out_filtered_msg->width = (uint32_t)in_selector.size();
  out_filtered_msg->height = 1;

  out_filtered_msg->fields = in_origin_cloud->fields;
  out_filtered_msg->header.frame_id = base_frame_;
  out_filtered_msg->header.stamp = in_origin_cloud->header.stamp;
  out_filtered_msg->point_step = in_origin_cloud->point_step;
  out_filtered_msg->row_step = point_size * in_selector.size();
  out_filtered_msg->is_dense = in_origin_cloud->is_dense && in_origin_cloud->data.size() == in_selector.size();
}

/*!
 * Classifies Points in the PointCoud as Ground and Not Ground
 * @param in_radial_ordered_clouds Vector of an Ordered PointsCloud ordered by radial distance from the origin
 * @param in_point_count Total number of lidar point. This is used to reserve the output's vector memory
 * @param out_ground_ptrs Returns the original adress of the points classified as ground in the original PointCloud
 * @param out_no_ground_ptrs Returns the original adress of the points classified as not ground in the original
 * PointCloud
 */
void RayGroundFilter::ClassifyPointCloud(const std::vector<PointCloudRH>& in_radial_ordered_clouds,
                                         const size_t in_point_count, std::vector<void*>* ground_block,
                                         std::vector<void*>* nonground_block)
{
  // todo: maybe it's necessary to add can_fit_result check

  for (auto& ray : in_radial_ordered_clouds)
  {
    reset_classifier();
    void* last_point_ptr = nullptr;
    RayGroundFilter::PointLabel last_label = RayGroundFilter::PointLabel::NONGROUND;
    for (auto& pt : ray)
    {
      const float z = pt.height;
      RayGroundFilter::PointLabel label;
      if (z >= max_height)
      {
        label = RayGroundFilter::PointLabel::NONGROUND;
      }
      else if (z <= min_height)
      {
        label = RayGroundFilter::PointLabel::GROUND;
      }
      else
      {
        label = RayGroundFilter::is_ground(pt);
      }

      // modify label of last point
      if (((label == RayGroundFilter::PointLabel::NONGROUND) &&
           (last_label == RayGroundFilter::PointLabel::PROVISIONAL_GROUND)) ||
          (label == RayGroundFilter::PointLabel::RETRO_NONGROUND))
      {
        last_label = RayGroundFilter::PointLabel::NONGROUND;
      }
      // push last point accordingly
      if (last_point_ptr != nullptr)
      {
        if (RayGroundFilter::label_is_ground(last_label))
        {
          ground_block->push_back(last_point_ptr);
        }
        else
        {
          nonground_block->push_back(last_point_ptr);
        }
      }
      // update state
      last_point_ptr = pt.original_data_pointer;
      last_label = label;
    }
    // push trailing point
    if (last_point_ptr != nullptr)
    {
      if (RayGroundFilter::label_is_ground(last_label))
      {
        ground_block->push_back(last_point_ptr);
      }
      else
      {
        nonground_block->push_back(last_point_ptr);
      }
    }
  }
}

void RayGroundFilter::reset_classifier()
{
  prev_radius = 0.0F;
  prev_height = 0.0F;
  last_was_ground = true;
  prev_ground_radius = 0.0F;
  prev_ground_height = 0.0F;
}

RayGroundFilter::PointLabel RayGroundFilter::is_ground(const PointRH& pt)
{
  PointLabel ret;
  const float height_m = pt.height;
  const float radius_m = pt.radius;

  // a small fudge factor is added because we check in the sorting process for "almost zero"
  // This is because points which are almost collinear are sorted by height
  const float dr_m = (radius_m - prev_radius) + FEPS;
  if (dr_m < 0.0F)
  {
    throw std::runtime_error("Ray Ground filter must receive points in increasing radius");
  }

  const float dh_m = fabsf(height_m - prev_height);

  const float local_slope_ratio = tan(deg2rad(max_local_slope_deg));
  const float general_slope_ratio = tan(deg2rad(max_global_slope_deg));

  const bool is_local = (dh_m < clamp(local_slope_ratio * dr_m, static_cast<float>(min_height_threshold),
                                      static_cast<float>(max_global_height_threshold)));
  const float global_height_thresh_m =
      std::min(general_slope_ratio * radius_m, static_cast<float>(max_global_height_threshold));
  const bool has_vertical_structure = (dh_m > (dr_m * nonground_retro_threshold));
  if (last_was_ground)
  {
    if (is_local)
    {
      // local in height, so ground
      ret = PointLabel::GROUND;
    }
    else if (has_vertical_structure)
    {
      // vertical structure, so nonground, need to retroactively annotate provisional gorund
      ret = PointLabel::RETRO_NONGROUND;
    }
    else
    {
      // check global cone
      ret = (fabsf(height_m) < global_height_thresh_m) ?
                PointLabel::GROUND :
                (dr_m < max_provisional_ground_distance) ? PointLabel::NONGROUND : PointLabel::NONLOCAL_NONGROUND;
    }
  }
  else
  {
    const float drg_m = (radius_m - prev_ground_radius);
    const float dhg_m = fabsf(height_m - prev_ground_height);
    const bool is_local_to_last_ground =
        (dhg_m <= clamp(local_slope_ratio * drg_m, static_cast<float>(min_height_threshold),
                        static_cast<float>(max_last_local_ground_threshold)));
    if (is_local_to_last_ground)
    {
      // local to last ground: provisional ground
      ret = PointLabel::PROVISIONAL_GROUND;
    }
    else if (is_local)
    {
      // local in height, so nonground
      ret = PointLabel::NONGROUND;
    }
    else
    {
      // global ground: provisionally ground
      ret = ((fabsf(height_m) < global_height_thresh_m)) ? PointLabel::PROVISIONAL_GROUND :
                                                                      PointLabel::NONGROUND;
    }
  }
  // update state
  last_was_ground = label_is_ground(ret);
  prev_radius = radius_m;
  prev_height = height_m;
  if (last_was_ground)
  {
    prev_ground_radius = radius_m;
    prev_ground_height = height_m;
  }

  return ret;
}

bool RayGroundFilter::label_is_ground(const RayGroundFilter::PointLabel label)
{
  return static_cast<int8_t>(label) <= static_cast<int8_t>(0);
}

float ReverseFloat(float inFloat)  // Swap endianness
{
  float retVal;
  char* floatToConvert = reinterpret_cast<char*>(&inFloat);
  char* returnFloat = reinterpret_cast<char*>(&retVal);

  // swap the bytes into a temporary buffer
  returnFloat[0] = floatToConvert[3];
  returnFloat[1] = floatToConvert[2];
  returnFloat[2] = floatToConvert[1];
  returnFloat[3] = floatToConvert[0];

  return retVal;
}

bool is_big_endian(void)
{
  union
  {
    uint32_t i;
    char c[4];
  } bint = { 0x01020304 };

  return bint.c[0] == 1;
}

/*!
 * Convert the sensor_msgs::PointCloud2 into PointCloudRH and filter out the points too high or too close
 * @param in_transformed_cloud Input Point Cloud to be organized in radial segments
 * @param in_min_distance Minimum valid distance, points closer than this will be removed.
 * @param out_radial_ordered_clouds Vector of Points Clouds, each element will contain the points ordered
 * @param out_no_ground_ptrs Returns the pointers to the points filtered out as no ground
 */
void RayGroundFilter::ConvertAndTrim(const sensor_msgs::PointCloud2::Ptr in_transformed_cloud, double in_min_distance,
                                     std::vector<PointCloudRH>* out_radial_ordered_clouds,
                                     std::vector<void*>* out_no_ground_ptrs)
{
  // --- Clarify some of the values used to access the binary blob
  size_t point_size = in_transformed_cloud->row_step / in_transformed_cloud->width;  // in Byte
  size_t cloud_count = in_transformed_cloud->width * in_transformed_cloud->height;

  const uint offset_not_set = ~0;
  uint x_offset = offset_not_set;  // in Byte from the point's start
  uint y_offset = offset_not_set;  // in Byte from the point's start
  uint z_offset = offset_not_set;  // in Byte from the point's start

  if (in_transformed_cloud->fields.size() < 3)
  {
    ROS_ERROR_STREAM_THROTTLE(10, "Failed to decode the pointcloud message : not enough fields found : "
                                      << in_transformed_cloud->fields.size() << " (needs at least 3 : x,y,z)");
    return;
  }

  for (uint i = 0; i < in_transformed_cloud->fields.size(); i++)
  {
    sensor_msgs::PointField field = in_transformed_cloud->fields[i];
    if ("x" == field.name)
    {
      x_offset = field.offset;
    }
    else if ("y" == field.name)
    {
      y_offset = field.offset;
    }
    else if ("z" == field.name)
    {
      z_offset = field.offset;
    }
  }

  if (offset_not_set == x_offset || offset_not_set == y_offset || offset_not_set == z_offset)
  {
    ROS_ERROR_STREAM_THROTTLE(10, "Failed to decode the pointcloud message : bad coordinate field name");
    return;
  }
  // ---

  out_radial_ordered_clouds->resize(radial_dividers_num);

  const int mean_ray_count = cloud_count / radial_dividers_num;
  // In theory reserving more than the average memory would reduce even more the number of realloc
  // but it would also make the reserving takes longer. One or two times the average are pretty
  // much identical in term of speedup. Three seems a bit worse.
  const int reserve_count = mean_ray_count;
  for (auto it = out_radial_ordered_clouds->begin(); it != out_radial_ordered_clouds->end(); it++)
  {
    it->reserve(reserve_count);
  }

  for (size_t i = 0; i < cloud_count; i++)
  {
    // --- access the binary blob fields
    uint8_t* point_start_ptr = reinterpret_cast<uint8_t*>(in_transformed_cloud->data.data()) + (i * point_size);
    float x = *(reinterpret_cast<float*>(point_start_ptr + x_offset));
    float y = *(reinterpret_cast<float*>(point_start_ptr + y_offset));
    float z = *(reinterpret_cast<float*>(point_start_ptr + z_offset));
    if (is_big_endian() != in_transformed_cloud->is_bigendian)
    {
      x = ReverseFloat(x);
      y = ReverseFloat(y);
      z = ReverseFloat(z);
    }
    // ---

    auto radius = static_cast<float>(sqrt(x * x + y * y));
    if (radius < in_min_distance)
    {
      out_no_ground_ptrs->push_back(point_start_ptr);
      continue;
    }
#ifdef USE_ATAN_APPROXIMATION
    auto theta = static_cast<float>(fast_atan2(y, x) * 180 / M_PI);
#else
    auto theta = static_cast<float>(atan2(y, x) * 180 / M_PI);
#endif  // USE_ATAN_APPROXIMATION
    if (theta < 0)
    {
      theta += 360;
    }
    else if (theta >= 360)
    {
      theta -= 360;
    }

    auto radial_div = (size_t)floor(theta / radial_divider_angle) % radial_dividers_num;
    out_radial_ordered_clouds->at(radial_div).emplace_back(z, radius, point_start_ptr);
  }  // end for

  // order radial points on each division
  auto strick_weak_radius_ordering = [](const PointRH& a, const PointRH& b) {
    if (a.radius < b.radius)
    {
      return true;
    }
    if (a.radius > b.radius)
    {
      return false;
    }
    // then the radius are equals. We add a secondary condition to keep the sort stable
    return a.original_data_pointer < b.original_data_pointer;
  };
  for (size_t i = 0; i < radial_dividers_num; i++)
  {
    std::sort(out_radial_ordered_clouds->at(i).begin(), out_radial_ordered_clouds->at(i).end(),
              strick_weak_radius_ordering);
  }
}

void RayGroundFilter::CloudCallback(const sensor_msgs::PointCloud2ConstPtr& in_sensor_cloud)
{
  boost::mutex::scoped_lock lock(mutex);
  health_checker_ptr_->NODE_ACTIVATE();
  health_checker_ptr_->CHECK_RATE("topic_rate_points_raw_slow", 8, 5, 1, "topic points_raw subscribe rate slow.");

  sensor_msgs::PointCloud2::Ptr trans_sensor_cloud(new sensor_msgs::PointCloud2);
  const bool succeeded = TransformPointCloud(base_frame_, in_sensor_cloud, trans_sensor_cloud);
  if (!succeeded)
  {
    ROS_ERROR_STREAM_THROTTLE(10,
                              "Failed transform from " << base_frame_ << " to " << in_sensor_cloud->header.frame_id);
    return;
  }

  std::vector<PointCloudRH> radial_ordered_clouds;
  std::vector<void*> ground_ptrs, no_ground_ptrs;
  ConvertAndTrim(trans_sensor_cloud, min_point_distance, &radial_ordered_clouds, &no_ground_ptrs);
  const size_t point_count = in_sensor_cloud->width * in_sensor_cloud->height;

  ClassifyPointCloud(radial_ordered_clouds, point_count, &ground_ptrs, &no_ground_ptrs);

  publish(ground_points_pub_, in_sensor_cloud, ground_ptrs);
  publish(groundless_points_pub_, in_sensor_cloud, no_ground_ptrs);
}

RayGroundFilter::RayGroundFilter() : node_handle_("~"), tf_listener_(tf_buffer_)
{
  ros::NodeHandle nh;
  ros::NodeHandle pnh("~");
  health_checker_ptr_ = std::make_shared<autoware_health_checker::HealthChecker>(nh, pnh);
  health_checker_ptr_->ENABLE();
}

void RayGroundFilter::reconfigure_callback(points_preprocessor::DynamicRayGroundFilterConfig& config, uint32_t level)
{
  boost::mutex::scoped_lock lock(mutex);
  if (level & MAX_GLOBAL_SLOPE_DEG_LEVEL)
  {
    max_global_slope_deg = config.max_global_slope_deg;
    ROS_INFO("max_global_slope_deg reconfigured: %f", config.max_global_slope_deg);
  }
  if (level & MAX_LOCAL_SLOPE_DEG_LEVEL)
  {
    max_local_slope_deg = config.max_local_slope_deg;
    ROS_INFO("max_local_slope_deg reconfigured: %f", config.max_local_slope_deg);
  }
  if (level & NONGROUND_RETRO_THRESH_DEG_LEVEL)
  {
    nonground_retro_threshold = tanf(static_cast<double>(deg2rad(config.nonground_retro_thresh_deg)));
    ROS_INFO("nonground_retro_thresh_deg reconfigured: %f", config.nonground_retro_thresh_deg);
  }
  if (level & MIN_HEIGHT_THRESHOLD_LEVEL)
  {
    min_height_threshold = config.min_height_threshold;
    ROS_INFO("min_height_threshold reconfigured: %f", config.min_height_threshold);
  }
  if (level & MAX_GLOBAL_HEIGHT_THRESHOLD_LEVEL)
  {
    max_global_height_threshold = config.max_global_height_threshold;
    ROS_INFO("max_global_height_threshold reconfigured: %f", config.max_global_height_threshold);
  }
  if (level & MAX_LAST_LOCAL_GROUND_THERSHOLD_LEVEL)
  {
    max_last_local_ground_threshold = config.max_last_local_ground_threshold;
    ROS_INFO("max_last_local_ground_threshold reconfigured: %f", config.max_last_local_ground_threshold);
  }
  if (level & MAX_PROVISIONAL_GROUND_DISTANCE_LEVEL)
  {
    max_provisional_ground_distance = config.max_provisional_ground_distance;
    ROS_INFO("max_provisional_ground_distance reconfigured: %f", config.max_provisional_ground_distance);
  }
  if (level & MIN_HEIGHT_LEVEL)
  {
    min_height = config.min_height;
    ROS_INFO("min_height reconfigured: %f", config.min_height);
  }
  if (level & MAX_HEIGHT_LEVEL)
  {
    max_height = config.max_height;
    ROS_INFO("max_height reconfigured: %f", config.max_height);
  }
  if (level & RADIAL_DIVIDER_ANGLE_LEVEL)
  {
    radial_divider_angle = config.radial_divider_angle;
    radial_dividers_num = ceil(360.0 / radial_divider_angle);
    ROS_INFO("radial_divider_angle reconfigured: %f", config.radial_divider_angle);
  }
  if (level & MIN_POINT_DISTANCE_LEVEL)
  {
    min_point_distance = config.min_point_distance;
    ROS_INFO("min_point_distance reconfigured: %f", config.min_point_distance);
  }
}

void RayGroundFilter::Run()
{
  // Model   |   Horizontal   |   Vertical   | FOV(Vertical)    degrees / rads
  // ----------------------------------------------------------
  // HDL-64  |0.08-0.35(0.32) |     0.4      |  -24.9 <=x<=2.0   (26.9  / 0.47)
  // HDL-32  |     0.1-0.4    |     1.33     |  -30.67<=x<=10.67 (41.33 / 0.72)
  // VLP-16  |     0.1-0.4    |     2.0      |  -15.0<=x<=15.0   (30    / 0.52)
  // VLP-16HD|     0.1-0.4    |     1.33     |  -10.0<=x<=10.0   (20    / 0.35)

  ROS_INFO("Initializing Ground Filter, please wait...");

  node_handle_.param<std::string>("input_point_topic", input_point_topic_, "/points_raw");

  std::string no_ground_topic, ground_topic;
  node_handle_.param<std::string>("no_ground_point_topic", no_ground_topic, "/points_no_ground");
  node_handle_.param<std::string>("ground_point_topic", ground_topic, "/points_ground");

  node_handle_.param<std::string>("base_frame", base_frame_, "base_link");

  node_handle_.param("max_global_slope_deg", max_global_slope_deg, 3.0);
  node_handle_.param("max_local_slope_deg", max_local_slope_deg, 5.0);

  double nonground_retro_threshold_deg;
  node_handle_.param("nonground_retro_thresh_deg", nonground_retro_threshold_deg, 20.0);
  nonground_retro_threshold = tanf(static_cast<double>(deg2rad(nonground_retro_threshold_deg)));

  node_handle_.param("min_height_threshold", min_height_threshold, 0.05);
  node_handle_.param("max_global_height_threshold", max_global_height_threshold, 1.5);
  node_handle_.param("max_last_local_ground_threshold", max_last_local_ground_threshold, 1.8);
  node_handle_.param("max_provisional_ground_distance", max_provisional_ground_distance, 2.0);
  node_handle_.param("min_height", min_height, -1.0);
  node_handle_.param("max_height", max_height, 2.0);

  node_handle_.param("radial_divider_angle", radial_divider_angle, 0.1);  // 0.1 degree default
  node_handle_.param("min_point_distance", min_point_distance, 1.85);     // 1.85 meters default

  radial_dividers_num = ceil(360.0 / radial_divider_angle);

  ROS_INFO("Input point_topic: %s", input_point_topic_.c_str());
  ROS_INFO("base_frame: %s", base_frame_.c_str());

  ROS_INFO("max_global_slope[deg]: %f", max_global_slope_deg);
  ROS_INFO("max_local_slope[deg]: %f", max_local_slope_deg);

  ROS_INFO("nonground_retro_threshold[deg]: %f", nonground_retro_threshold_deg);
  ROS_INFO("min_height_threshold[meters]: %f", min_height_threshold);
  ROS_INFO("max_global_height_threshold[meters]: %f", max_global_height_threshold);
  ROS_INFO("max_last_local_ground_threshold[meters]: %f", max_last_local_ground_threshold);
  ROS_INFO("max_provisional_ground_distance[meters]: %f", max_provisional_ground_distance);
  ROS_INFO("min_height[meters]: %f", min_height);
  ROS_INFO("max_height[meters]: %f", max_height);

  ROS_INFO("radial_divider_angle[deg]: %f", radial_divider_angle);
  ROS_INFO("min_point_distance[meters]: %f", min_point_distance);

  ROS_INFO("Radial Divisions: %d", (int)radial_dividers_num);
  ROS_INFO("No Ground Output Point Cloud no_ground_point_topic: %s", no_ground_topic.c_str());
  ROS_INFO("Only Ground Output Point Cloud ground_topic: %s", ground_topic.c_str());
  ROS_INFO("Subscribing to... %s", input_point_topic_.c_str());

  points_node_sub_ = node_handle_.subscribe(input_point_topic_, 1, &RayGroundFilter::CloudCallback, this);
  config_node_sub_ =
      node_handle_.subscribe("/config/ray_ground_filter", 1, &RayGroundFilter::update_config_params, this);

  groundless_points_pub_ = node_handle_.advertise<sensor_msgs::PointCloud2>(no_ground_topic, 2);
  ground_points_pub_ = node_handle_.advertise<sensor_msgs::PointCloud2>(ground_topic, 2);

  dynamic_reconfigure::Server<points_preprocessor::DynamicRayGroundFilterConfig> server;
  dynamic_reconfigure::Server<points_preprocessor::DynamicRayGroundFilterConfig>::CallbackType f;

  f = boost::bind(&RayGroundFilter::reconfigure_callback, this, _1, _2);
  server.setCallback(f);

  ROS_INFO("Ready");

  ros::spin();
}
